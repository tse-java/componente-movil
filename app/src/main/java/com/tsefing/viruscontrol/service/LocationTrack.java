package com.tsefing.viruscontrol.service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import androidx.core.app.ActivityCompat;
import com.tsefing.viruscontrol.controller.LocationController;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import org.apache.commons.lang3.StringUtils;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import uy.viruscontrol.common.dto.DTODistance;
import uy.viruscontrol.common.dto.DTOLocationReport;
import uy.viruscontrol.common.exceptions.VirusControlException;

public class LocationTrack extends Service implements LocationListener {

    private final Context mContext;

    boolean checkGPS = false;
    boolean checkNetwork = false;

    Location loc;
    double latitude;
    double longitude;
    Location lastLocation = null;
    int contador = 0;
    float distanceReport = 0;

    double distance;
    double speed;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 3;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 2;
    protected LocationManager locationManager;
    private boolean isManualReport = true;
    private static LocationController locationController;
    private static DTODistance dtoContagionDistance;
    private Double contagionDistance = 15.0;

    private static LocationTrack instance;

    public static LocationTrack getInstance() {
        if (instance == null)
            throw new NullPointerException("Please call initialize() before getting the instance.");
        return instance;
    }

    public synchronized static void initialize(Context applicationContext) {
        if (applicationContext == null)
            throw new NullPointerException("Provided application context is null");
        else if (instance == null) {
            locationController = new LocationController();
            getContagionDistance();
            instance = new LocationTrack(applicationContext, true);

        }
    }

    ExecutorService mThreadPool = Executors.newSingleThreadExecutor();

    public LocationTrack(Context mContext, Boolean isManualReport) {
        this.isManualReport = isManualReport;
        this.mContext = mContext;
    }

    public void reportLocation() throws VirusControlException {
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        checkGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        checkNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            throw new VirusControlException("Permiso para gps", -1);
        } else if (!checkGPS && !checkNetwork) {
            throw new VirusControlException("Activar servicios", -2);
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            if (locationManager != null) {
                loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (loc != null) {
                    latitude = loc.getLatitude();
                    longitude = loc.getLongitude();
                    speed = loc.getSpeed();
                    if (isManualReport) {
                        handleManualReport(loc);
                    }
                }
            }
        }
    }

    public void stopListener() {
        if (locationManager != null) {
            locationManager.removeUpdates(LocationTrack.this);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        mThreadPool.execute(new Runnable() {
            @Override
            public void run() {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT-3"));
                String time = sdf.format(location.getTime());

                if (loc != null && !isManualReport) {
                    if (lastLocation == null) {
                        lastLocation = location;
                        distance = 0;
                        speed = 0;
                    } else {
                        distance = lastLocation.distanceTo(location);
                        speed = distance / ((location.getTime() - lastLocation.getTime()) / 1000);
                        distanceReport = distanceReport + (lastLocation.distanceTo(location));
                        lastLocation = location;
                    }

                    Log.d("ULTIMA UBICACION", contador + ") Ultima ubicacion: " + lastLocation.getLatitude() + " | " + lastLocation.getLongitude());

                    contador++;

                    if (dtoContagionDistance != null) {
                        contagionDistance = dtoContagionDistance.getDistance();
                    }

                    //sendInfoToReportLocationActivity(speed, distance);
                    if (distanceReport > contagionDistance && speed < 8 && speed > 1 && contador > 5) {
                        sendReport(location);
                        Log.d("REPORTE AUTO", "Dentro de los parametros para el reporte automatico");
                        distanceReport = 0;
                    }
                }
            }
        });
    }

    public void handleManualReport(Location location) {
        sendReport(location);
        Log.d("REPORTE MANUAL", "Latitud: " + location.getLatitude() + " | Longitud: " + location.getLongitude());
        distanceReport = 0;
        stopListener();
    }

    private void sendReport(Location location){
        try {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            String stateName = addresses.get(0).getAdminArea();
            if (addresses.get(0).getCountryCode().contentEquals("UY")) {
                stateName = StringUtils.stripAccents(stateName.substring(16).toUpperCase().replaceAll(" ", "_"));
            }
            DTOLocationReport dtoLocationReport = new DTOLocationReport();
            dtoLocationReport.setDate(new Date());
            dtoLocationReport.setLatitude(location.getLatitude());
            dtoLocationReport.setLongitude(location.getLongitude());
            dtoLocationReport.setDepartment(stateName);
            LocationController locationController = new LocationController();
            locationController.reportLocation(dtoLocationReport)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(aVoid -> {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void getContagionDistance(){
        locationController.getContagionDistance()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DTODistance>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("VIRUSCONTROL", "Aguarde...");
                    }

                    @Override
                    public void onSuccess(DTODistance dtoDistance) {
                        dtoContagionDistance = dtoDistance;
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("VIRUSCONTROL", "Error: " + e.getMessage());
                    }
                });
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) { }

    @Override
    public void onProviderDisabled(String s) { }

    public void setManualReport(boolean manualReport) {
        isManualReport = manualReport;
    }
}
