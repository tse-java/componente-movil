package com.tsefing.viruscontrol.service;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tsefing.viruscontrol.R;
import java.util.Map;
import java.util.Random;

public class FirebaseService extends FirebaseMessagingService {
    private LocalBroadcastManager broadcaster;
    private final String TAG = "VIRUSCONTROL";

    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d(TAG, "incomming token: " + s);
    }

    public FirebaseService() { }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        Handler handler = new Handler(Looper.getMainLooper());

        Intent intent = new Intent("updateUnread");

        Map<String, String> body = remoteMessage.getData();
        String type = body.get("type");

        int notificationId = new Random().nextInt(60000);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        handler.post(() -> {
            if (type.equalsIgnoreCase("exam")) {
                createCustomToast(R.layout.toast_exam);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "exam")
                        .setSmallIcon(R.drawable.exam_result)
                        .setContentTitle(remoteMessage.getData().get("title")) //the "title" value you sent in your notification
                        .setContentText(remoteMessage.getData().get("message")) //ditto
                        .setAutoCancel(true)  //dismisses the notification on click
                        .setSound(defaultSoundUri);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());
            } else if (type.equalsIgnoreCase("medic")) {
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "medic")
                        .setSmallIcon(R.drawable.appointment)
                        .setContentTitle(remoteMessage.getData().get("title")) //the "title" value you sent in your notification
                        .setContentText(remoteMessage.getData().get("message")) //ditto
                        .setAutoCancel(true)  //dismisses the notification on click
                        .setSound(defaultSoundUri);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());
                createCustomToast(R.layout.toast_appointment);
            } else if (type.equalsIgnoreCase("contagion")) {
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "contagion")
                        .setSmallIcon(R.drawable.virus_alert)
                        .setContentTitle(remoteMessage.getData().get("title")) //the "title" value you sent in your notification
                        .setContentText(remoteMessage.getData().get("message")) //ditto
                        .setAutoCancel(true)  //dismisses the notification on click
                        .setSound(defaultSoundUri);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());
                createCustomToast(R.layout.toast_contagion);
            }
            else if (type.equalsIgnoreCase("resource")) {
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "resource")
                        .setSmallIcon(R.drawable.resources)
                        .setContentTitle(remoteMessage.getData().get("title")) //the "title" value you sent in your notification
                        .setContentText(remoteMessage.getData().get("message")) //ditto
                        .setAutoCancel(true)  //dismisses the notification on click
                        .setSound(defaultSoundUri);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());
                createCustomToast(R.layout.toast_resource);
            }
        });
        broadcaster.sendBroadcast(intent);

    }

    private void createCustomToast(int layoutCustomToast) {
        Context context = getApplicationContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View toastRoot = inflater.inflate(getResources().getLayout(layoutCustomToast), null);
        Toast toast = new Toast(context);

        toast.setView(toastRoot);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 150);
        toast.show();
    }
}
