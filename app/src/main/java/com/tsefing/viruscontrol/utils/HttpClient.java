package com.tsefing.viruscontrol.utils;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class HttpClient {
    private static final OkHttpClient client = new OkHttpClient()
            .newBuilder()
            .addInterceptor(chain -> {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + Session.getInstace().getLogin().getAccessToken())
                        .build();
                return chain.proceed(newRequest);
            })
            .authenticator(new TokenAuthenticator()).
                    build();

    public static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.viruscontrol.tech")
            .addConverterFactory(JacksonConverterFactory.create())
            .client(client)
            .build();

    public HttpClient() { }
}