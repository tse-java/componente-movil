package com.tsefing.viruscontrol.utils;

import androidx.annotation.Nullable;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import okhttp3.Authenticator;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;
import uy.viruscontrol.common.dto.DTOLogin;
import uy.viruscontrol.common.dto.DTOToken;
import uy.viruscontrol.common.exceptions.VirusControlException;

public class TokenAuthenticator implements Authenticator {

    @Nullable
    @Override
    public Request authenticate(@Nullable Route route, Response response) throws IOException {
        if (response.code() == 401) {
            try{
                DTOLogin refreshCall = refreshAccessToken();
                Session.getInstace().setLogin(refreshCall);
                return response.request().newBuilder()
                        .header("Authorization", "Bearer "+ refreshCall.getAccessToken())
                        .build();
            }catch (VirusControlException e){
                 return null;
            }
        }
        return null;
    }

    private DTOLogin refreshAccessToken() throws VirusControlException {
        final OkHttpClient client = new OkHttpClient();
        final ObjectMapper objectMapper = new ObjectMapper();

        DTOToken refreshTokenDTO = new DTOToken();
        refreshTokenDTO.setToken(Session.getInstace().getLogin().getRefreshToken());

        try {
            String refreshTokenJson = objectMapper.writeValueAsString(refreshTokenDTO);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), refreshTokenJson);
            Request request = new Request.Builder()
                    .url("https://api.viruscontrol.tech/users/refreshToken")
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            String virusJson = response.body().string();
            return objectMapper.readValue(virusJson, DTOLogin.class);
        } catch (IOException e) {
            throw new VirusControlException("Error al refrescar token", -1);
        }
    }
}