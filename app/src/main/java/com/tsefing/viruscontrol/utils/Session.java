package com.tsefing.viruscontrol.utils;

import uy.viruscontrol.common.dto.DTOFrontProfile;
import uy.viruscontrol.common.dto.DTOLogin;

public class Session {
    private static Session instace = null;
    private DTOLogin login = new DTOLogin();
    private DTOFrontProfile profile = new DTOFrontProfile();

    private Session(){ }

    public static Session getInstace() {
        if(instace == null){
            instace = new Session();
        }
        return instace;
    }

    public DTOLogin getLogin() {
        return login;
    }

    public void setLogin(DTOLogin login) {
        this.login = login;
    }

    public void setProfile(DTOFrontProfile dtoProfile) {
        this.profile = dtoProfile;
    }

    public DTOFrontProfile getProfile() { return profile; }
}