package com.tsefing.viruscontrol.ui;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.tsefing.viruscontrol.R;
import com.tsefing.viruscontrol.utils.Session;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uy.viruscontrol.common.dto.DTOAppointmentNotification;
import uy.viruscontrol.common.dto.DTOContagionNotification;
import uy.viruscontrol.common.dto.DTOExamNotification;
import uy.viruscontrol.common.dto.DTOResourceNotification;

public class NotificationActivity extends AppCompatActivity {
    private List<NotificationItem> mNotifications;
    private NotificationAdapter mNotificationAdapter;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final String TAG = "VIRUSCONTROL";
    private Button mBtnClearNotifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mNotifications = Collections.synchronizedList(new ArrayList<>());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        fetchNotifications();
        generarNotificacionesAlPedo();
        mBtnClearNotifications = findViewById(R.id.btn_clear_notifications);
        mBtnClearNotifications.setOnClickListener(v -> {
            new AlertDialog.Builder(this)
                    .setTitle("Eliminar notificaciones")
                    .setMessage("Esta seguro que desea eliminar todas las notificaciones?")
                    .setPositiveButton(R.string.confirmation, (dialog, which) -> {
                        int i = 0;
                        while (mNotifications.size() > 0) {
                            i++;
                            removeNotification(0);
                        }
                    })
                    .setNegativeButton(R.string.negation, null)
                    .setIcon(R.drawable.attention)
                    .show();

        });
    }

    private void generarNotificacionesAlPedo() {
        RecyclerView mRecyclerNotifications = findViewById(R.id.rv_notifications);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mNotificationAdapter = new NotificationAdapter(this, mNotifications);
        mRecyclerNotifications.setLayoutManager(mLayoutManager);
        mRecyclerNotifications.setAdapter(mNotificationAdapter);
        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback(mNotificationAdapter));
        itemTouchHelper.attachToRecyclerView(mRecyclerNotifications);
        mNotificationAdapter.setOnItemClickListener(this::removeNotification);
    }

    public void removeNotification(int position) {
        Log.d(TAG, "removeNotification");
        NotificationItem item = mNotifications.get(position);
        mNotifications.remove(position);
        Map<String, Object> updates = new HashMap<>();
        updates.put("read", true);
        db.collection(item.getType()).document(item.getId())
                .update(updates)
                .addOnSuccessListener(aVoid -> Log.d(TAG, "DocumentSnapshot successfully deleted!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error deleting document", e));
        mNotificationAdapter.notifyItemRemoved(position);
    }

    private void fetchNotifications() {
        //NOTIFICACION DE RESULTADO/ESTADO DE EXAMEN
        String externalId = Session.getInstace().getProfile().getExternalId();
        mNotifications.clear();
        db.collection("examNotifications")
                .whereEqualTo("externalId", externalId)
                .whereEqualTo("read", false)
                .get()
                .addOnCompleteListener(command -> {
                    QuerySnapshot querySnapshot = command.getResult();
                    for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                        DTOExamNotification notification = document.toObject(DTOExamNotification.class);
                        NotificationItem item;
                        if (notification.getStatus().equalsIgnoreCase("delivered")) {
                            String result = notification.getIsPositive() ? "Positivo" : "Negativo";
                            item = new NotificationItem(R.drawable.exam_result,
                                    "Examen para: " + notification.getDiseaseName() + "\n" +
                                            "Estado: Completo\n" +
                                            "Resultado: " + result,
                                    "examNotifications", document.getId());

                        } else {
                            String status = notification.getStatus().equalsIgnoreCase("PENDING") ? "Pendiente" : "En Proceso";
                            item = new NotificationItem(R.drawable.exam_result,
                                    "Examen para: " + notification.getDiseaseName() + "\n" +
                                            "Estado: " + status,
                                    "examNotifications", document.getId());

                        }
                        mNotifications.add(item);
                    }
                    mNotificationAdapter.notifyDataSetChanged();
                });

        //ALERTA DE CONTAGIO
        db.collection("contagionNotifications")
                .whereEqualTo("externalId", externalId)
                .whereEqualTo("read", false)
                .get().addOnCompleteListener(command -> {
            QuerySnapshot querySnapshot = command.getResult();
            for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                DTOContagionNotification notification = document.toObject(DTOContagionNotification.class);
                NotificationItem item = new NotificationItem(R.drawable.virus_alert,
                        "Usted es ahora considerado un caso sospecho de: \n" + notification.getDiseaseName(),
                        "contagionNotifications", document.getId());
                mNotifications.add(item);

            }
            mNotificationAdapter.notifyDataSetChanged();
        });

        //CONSULTA MEDICA ASIGNADA
        db.collection("appointmentNotifications")
                .whereEqualTo("externalId", externalId)
                .whereEqualTo("read", false)
                .get().addOnCompleteListener(command -> {
            QuerySnapshot querySnapshot = command.getResult();
            for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                DTOAppointmentNotification notification = document.toObject(DTOAppointmentNotification.class);
                NotificationItem notificationItem = new NotificationItem(R.drawable.appointment,
                        "Usted ha sido agendando para: " + notification.getDate() + "\n" +
                                "Medico: " + notification.getDoctorName(),
                        "appointmentNotifications", document.getId());
                mNotifications.add(notificationItem);
            }
            mNotificationAdapter.notifyDataSetChanged();
        });

        //DISPONIBILIDAD DE RECURSO
        db.collection("resourceNotifications").
                whereEqualTo("externalId", externalId)
                .whereEqualTo("read", false)
                .get()
                .addOnCompleteListener(command -> {
                    QuerySnapshot querySnapshot = command.getResult();
                    for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                        DTOResourceNotification notification = document.toObject(DTOResourceNotification.class);
                        mNotifications.add(new NotificationItem(R.drawable.resources,
                                "Nueva disponibilidad del recurso: " + notification.getResource().getName(),
                                "resourceNotifications", document.getId()));
                    }
                    mNotificationAdapter.notifyDataSetChanged();
                });
    }
}
