package com.tsefing.viruscontrol.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.tsefing.viruscontrol.R;
import com.tsefing.viruscontrol.controller.UserController;
import com.tsefing.viruscontrol.utils.Session;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import uy.viruscontrol.common.dto.DTOToken;

public class LoginActivity extends AppCompatActivity {

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    private final String TAG = "VIRUSCONTROL";
    private GoogleSignInClient mGoogleSignInClient;
    private UserController userController = new UserController();
    private static final int REQUEST_LOCATION = 123;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode("884442857108-q9a1vj0f8r97ku0e2bb3de8kprmb1del.apps.googleusercontent.com")
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        siguienteActividad();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void siguienteActividad() {
        Button loginBtn = findViewById(R.id.button_login);
        loginBtn.setOnClickListener(view -> signIn());
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount acct = completedTask.getResult(ApiException.class);
            if(acct != null){
                String authCode = acct.getServerAuthCode();
                SharedPreferences sharedPref = this.getSharedPreferences("viruscontrol", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                compositeDisposable.add(
                        userController.googleLogin(authCode).flatMap(googleAccessToken -> {
                            DTOToken dtoToken = new DTOToken();
                            dtoToken.setToken(googleAccessToken);
                            return userController.viruscontrolLogin(dtoToken);
                        }).flatMap(dtoLogin -> {
                            Session.getInstace().setLogin(dtoLogin);
                            String dtoLoginJson = objectMapper.writeValueAsString(dtoLogin);
                            editor.putString("dtoLogin", dtoLoginJson);
                            editor.apply();
                            return userController.getProfile();
                        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                                .subscribe((dtoFrontProfile, throwable) -> {
                                    if (throwable != null) {
                                        Toast toast = Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT);
                                        toast.setMargin(50, 50);
                                        toast.show();
                                        Log.i(TAG, "Error de login: " + throwable.getMessage());
                                    } else {
                                        Toast toast = Toast.makeText(getApplicationContext(), "Session iniciada", Toast.LENGTH_SHORT);
                                        toast.setMargin(50, 50);
                                        toast.show();
                                        Session.getInstace().setProfile(dtoFrontProfile);
                                        String dtoProfileJson = objectMapper.writeValueAsString(dtoFrontProfile);
                                        editor.putString("dtoProfile", dtoProfileJson);
                                        editor.commit();
                                        goHomeActivity();
                                    }
                                }));
            }else{
                Log.e(TAG, "handleSignInResult: algo salio mal?" );
            }

        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 1);
    }

    private void goHomeActivity() {
        Intent i = new Intent(this, HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}