package com.tsefing.viruscontrol.ui;

public class NotificationItem {
    private int mIcon;
    private String mNotification;
    private String type;
    private String id;

    public NotificationItem() { }

    public NotificationItem(int mIcon, String mNotification, String type, String id) {
        this.mIcon = mIcon;
        this.mNotification = mNotification;
        this.type = type;
        this.id = id;
    }

    public int getIcon() { return mIcon; }

    public String getNotification() { return mNotification; }

    public String getType() {
        return type;
    }

    public String getId() { return id; }
}
