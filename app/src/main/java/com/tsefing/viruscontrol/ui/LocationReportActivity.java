package com.tsefing.viruscontrol.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.content.*;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.os.Bundle;
import com.tsefing.viruscontrol.R;
import com.tsefing.viruscontrol.service.LocationTrack;
import uy.viruscontrol.common.exceptions.VirusControlException;

public class LocationReportActivity extends AppCompatActivity {
    private static final String TAG = "VIRUSCONTROL";
    private Switch mSwitchLocationReport;
    private CheckBox mCheckAutoReport, mCheckManualReport;
    private TextView mTextAutoReport, mTextManualReport;
    private Button mSendLocation;
    private static final int REQUEST_LOCATION = 123;
    private LocationTrack locationTrack;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref  =this.getSharedPreferences("viruscontrol", Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        setContentView(R.layout.activity_report_location);

        mSwitchLocationReport = findViewById(R.id.switch_location);
        mCheckAutoReport = findViewById(R.id.check_auto_report);
        mCheckManualReport = findViewById(R.id.check_manual_report);
        mTextAutoReport = findViewById(R.id.tv_auto_report);
        mTextManualReport = findViewById(R.id.tv_manual_report);
        mSendLocation = findViewById(R.id.btn_send_report);

        locationTrack = LocationTrack.getInstance();

        switchState();
        checkReportMode();
        mSendLocation.setOnClickListener(v -> {
            handleLocation(true);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isOn = sharedPref.getBoolean("isOn", true);
        boolean isManual = sharedPref.getBoolean("isManual", true);
        mSwitchLocationReport.setChecked(isOn);
        mCheckManualReport.setChecked(isManual);
        mCheckAutoReport.setChecked(!isManual);
        if(!isOn || !isManual){
            mSendLocation.setEnabled(false);
            if (isOn) {
                handleLocation(false);
            }
        }
    }

    private void switchState() {
        if (mSwitchLocationReport != null) {
            mSwitchLocationReport.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    //Si el SWITCH esta en ON
                    mCheckAutoReport.setEnabled(true);
                    mCheckManualReport.setEnabled(true);
                    mTextAutoReport.setEnabled(true);
                    mTextManualReport.setEnabled(true);
                    if (mCheckManualReport.isChecked()){
                        mSendLocation.setEnabled(true);
                        locationTrack.setManualReport(true);
                    }else {
                        mSendLocation.setEnabled(false);
                        if(locationTrack!=null){
                            handleLocation(false);
                        }
                    }

                    editor.putBoolean("isManual", !mCheckAutoReport.isChecked());
                    editor.putBoolean("isOn", true);
                } else {
                    //Si el SWITCH esta en OFF
                    if(locationTrack!=null){
                        locationTrack.setManualReport(true);
                        locationTrack.stopListener();
                    }
                    mCheckAutoReport.setEnabled(false);
                    mCheckManualReport.setEnabled(false);
                    mTextAutoReport.setEnabled(false);
                    mTextManualReport.setEnabled(false);
                    mSendLocation.setEnabled(false);
                    editor.putBoolean("isManual", !mCheckAutoReport.isChecked());
                    editor.putBoolean("isOn", false);
                }
                editor.commit();
            });
        }
    }

    private void checkReportMode() {
        //Listeners para opcion de modo de reporte
        //mTextAutoReport por si toca el textview, mCheckAutoReport por si toca el checkbox
        checkAutoReport(mTextAutoReport);
        checkAutoReport(mCheckAutoReport);
        //idem a lo anterior pero con el caso de "reporte manual"
        checkManualReport(mTextManualReport);
        checkManualReport(mCheckManualReport);
    }

    private void checkAutoReport(View checkAuto) {
        checkAuto.setOnClickListener(v -> {
            mCheckAutoReport.setChecked(true);
            mCheckManualReport.setChecked(false);
            mSendLocation.setEnabled(false);
            if (locationTrack != null) {
                handleLocation(false);
            }
            editor.putBoolean("isManual", !mCheckAutoReport.isChecked());
            editor.commit();
        });
    }

    private void checkManualReport(View checkManual) {
        checkManual.setOnClickListener(v -> {
            mCheckAutoReport.setChecked(false);
            mCheckManualReport.setChecked(true);
            mSendLocation.setEnabled(true);
            if (locationTrack != null) {
                locationTrack.setManualReport(true);
                locationTrack.stopListener();
            }
            editor.putBoolean("isManual", !mCheckAutoReport.isChecked());
            editor.commit();
        });
    }

    private void handleLocation(Boolean isManual) {
        try {
            locationTrack.setManualReport(isManual);
            locationTrack.reportLocation();
        } catch (VirusControlException e) {
            if (e.getCode() == -1) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "get permissions! ");
                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    }, REQUEST_LOCATION);
                }
            } else {
                askGPSAccess();
            }
        }
    }

    private void askGPSAccess() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle(getResources().getString(R.string.dialog_location_service_required));
        alertDialog.setMessage(getResources().getString(R.string.dialog_location_service_ask));

        alertDialog.setPositiveButton(getResources().getString(R.string.confirmation), (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            this.startActivity(intent);
        });

        alertDialog.setNegativeButton(getResources().getString(R.string.negation), (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }
}
