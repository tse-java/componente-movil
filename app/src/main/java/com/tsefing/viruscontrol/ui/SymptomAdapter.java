package com.tsefing.viruscontrol.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.tsefing.viruscontrol.R;
import java.util.ArrayList;

public class SymptomAdapter extends RecyclerView.Adapter<SymptomAdapter.SymptomViewHolder> {
    private ArrayList<SymptomItem> mSymptomsList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onDeleteClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public static class SymptomViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public ImageView mDeleteButton;

        public SymptomViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.tv_selected_symptom);
            mDeleteButton = itemView.findViewById(R.id.iv_delete_symptom);

            mDeleteButton.setOnClickListener(v -> {
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION){
                        listener.onDeleteClick(position);
                    }
                }
            });
        }
    }

    public SymptomAdapter(ArrayList<SymptomItem> symptomsList){
        mSymptomsList = symptomsList;
    }

    @NonNull
    @Override
    public SymptomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_symptom_add_item, parent, false);
        return new SymptomViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull SymptomViewHolder holder, int position) {
        SymptomItem currentSymptom = mSymptomsList.get(position);
        holder.mTextView.setText(currentSymptom.getSymptom());
    }

    @Override
    public int getItemCount() {
        return mSymptomsList.size();
    }
}
