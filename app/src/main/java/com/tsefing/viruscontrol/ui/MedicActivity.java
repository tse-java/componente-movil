package com.tsefing.viruscontrol.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.tsefing.viruscontrol.R;
import com.tsefing.viruscontrol.controller.SymptomController;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import uy.viruscontrol.common.dto.DTOSymptom;
import uy.viruscontrol.common.exceptions.VirusControlException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MedicActivity extends AppCompatActivity {
    private final SymptomController symptomController = new SymptomController();
    private Map<Long, String> mSymptoms = new HashMap<>();

    private AutoCompleteTextView mAutoTextSymptoms;
    //Titulo encima de lista de sintomas del usuario
    private TextView mTvSelectedSymptoms;
    private ArrayList<Long> mSymptomsID = new ArrayList<>();
    //Lista de objetos para mostrar en el RecyclerView
    private ArrayList<SymptomItem> mSymptomItems= new ArrayList<SymptomItem>();
    //Adaptador custom para la RecyclerView y los SymptomItem
    private SymptomAdapter mAdapterChosenSymtoms;
    private Button mRequestMedic;
    private InputMethodManager imm;

    public MedicActivity() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medic);
        mTvSelectedSymptoms = findViewById(R.id.tv_selected_symptoms);
        mRequestMedic = findViewById(R.id.btn_request_medic);
        imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        handleGetSymptoms();

        buildSymptomsRecyclerView();

        mRequestMedic.setOnClickListener(v -> {
            Intent i = new Intent(this, AddressActivity.class);
            i.putExtra("mSymptomsList", mSymptomsID);
            startActivity(i);
        });
    }

    private void handleGetSymptoms() {
        symptomController.getSymptoms()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DTOSymptom>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("VIRUSCONTROL", "Aguarde...");
                    }

                    @Override
                    public void onSuccess(List<DTOSymptom> dtoSymptoms) {
                        getSymptomsApi(dtoSymptoms);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("VIRUSCONTROL", "Error: " + e.getMessage());
                        if (e instanceof VirusControlException){
                            VirusControlException vExc = (VirusControlException) e;
                            if (vExc.getCode() == 401){
                                goLogin();
                            }
                        }
                    }
                });

    }

    private void getSymptomsApi(List<DTOSymptom> dtoSymptoms){
        for (DTOSymptom dtoSymptom: dtoSymptoms){
            mSymptoms.put(dtoSymptom.getId(), dtoSymptom.getName());
        }
        buildAutoCompleteSymptomsList();
    }

    private void goLogin(){
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }

    @SuppressLint("ClickableViewAccessibility")
    public void buildAutoCompleteSymptomsList(){
        mAutoTextSymptoms = findViewById(R.id.tv_symptoms);
        List<String> values = new ArrayList<>(mSymptoms.values());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.custom_symptom_list_item, R.id.text_view_list_item, values);
        mAutoTextSymptoms.setAdapter(adapter);

        mAutoTextSymptoms.setOnTouchListener((v, event) -> {
            mAutoTextSymptoms.showDropDown();
            return false;
        });

        mAutoTextSymptoms.setOnItemClickListener((parent, view, position, id) -> insertSymptomItem());
    }

    public void insertSymptomItem(){
        String symptom = mAutoTextSymptoms.getText().toString();
        boolean contains = false;
        if (mSymptoms.containsValue(symptom)){
            for (SymptomItem si: mSymptomItems){ //Me fijo si ya lo eligio el usuario
                if (si.getSymptom().contentEquals(symptom)){
                    contains = true;
                    break;
                }
            }
            if (!contains){ //Si no lo eligio, lo agrego
                Long id = getSymtomID(symptom);
                if (!mSymptomsID.contains(id)) {
                    mSymptomsID.add(id);
                }

                mSymptomItems.add(new SymptomItem(symptom, R.drawable.ic_cross));
                imm.hideSoftInputFromWindow(mAutoTextSymptoms.getApplicationWindowToken(), 0);
                mTvSelectedSymptoms.setVisibility(View.VISIBLE);
                mRequestMedic.setVisibility(View.VISIBLE);
            }
            mAutoTextSymptoms.setText("");
        }
    }

    private Long getSymtomID(String symptom){
        for (Map.Entry<Long, String> entry : mSymptoms.entrySet()) {
            if (entry.getValue().equals(symptom)) {
                return entry.getKey();
            }
        }
        return null;
    }

    public void buildSymptomsRecyclerView(){
        //Elemento en pantalla de lista de sintomas elegidos por el usuario
        RecyclerView mRvUserSymptomsList = findViewById(R.id.rv_selected_symptoms);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mAdapterChosenSymtoms = new SymptomAdapter(mSymptomItems);
        mRvUserSymptomsList.setLayoutManager(mLayoutManager);
        mRvUserSymptomsList.setAdapter(mAdapterChosenSymtoms);
        mAdapterChosenSymtoms.setOnItemClickListener(this::removeSymptom);
    }

    public void removeSymptom(int position) {
        String symptom = mSymptomItems.get(position).getSymptom();
        mSymptomItems.remove(position);
        mAdapterChosenSymtoms.notifyItemRemoved(position);
        Long id = getSymtomID(symptom);
        mSymptomsID.remove(id);
        if (mSymptomItems.size() == 0) {
            mRequestMedic.setVisibility(View.GONE);
            mTvSelectedSymptoms.setVisibility(View.INVISIBLE);
        }
    }
}
