package com.tsefing.viruscontrol.ui;

public class SymptomItem {
    private String mSymptom;
    private int mDeleteButton;

    public SymptomItem() {}

    public SymptomItem(String symptom, int deleteButton) {
        this.mSymptom = symptom;
        this.mDeleteButton = deleteButton;
    }

    public String getSymptom() {
        return mSymptom;
    }

    public int getDeleteButton() {
        return mDeleteButton;
    }

}
