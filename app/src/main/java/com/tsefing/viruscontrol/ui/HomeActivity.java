package com.tsefing.viruscontrol.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.tsefing.viruscontrol.R;
import com.tsefing.viruscontrol.controller.UserController;
import com.tsefing.viruscontrol.service.LocationTrack;
import com.tsefing.viruscontrol.utils.LanguageChange;
import com.tsefing.viruscontrol.utils.Session;
import java.util.Arrays;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import uy.viruscontrol.common.dto.DTOFrontProfile;
import uy.viruscontrol.common.dto.DTOLogin;
import uy.viruscontrol.common.exceptions.VirusControlException;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "VIRUSCONTROL";
    private SharedPreferences sharedPref;
    private ObjectMapper objectMapper = new ObjectMapper();
    private UserController userController = new UserController();
    private TextView mNotificationNum;
    private ImageButton cardMedic;
    private ImageButton cardLocation;
    private ImageButton cardNotification;
    private ImageButton cardSettings;
    private int unreadNotifications = 0;
    private static final int REQUEST_LOCATION = 123;
    private LocationTrack locationTrack;

    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            unreadNotifications++;
            mNotificationNum.setText(String.valueOf(unreadNotifications));
        }
    };

    @Override
   protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((myReceiver),
                new IntentFilter("updateUnread")
        );
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mNotificationNum = findViewById(R.id.notification_num);
        cardMedic = findViewById(R.id.card_medic);
        cardLocation = findViewById(R.id.card_location);
        cardNotification = findViewById(R.id.card_notification);
        cardSettings = findViewById(R.id.card_settings);
        cardMedic.setOnClickListener(this);
        cardLocation.setOnClickListener(this);
        cardNotification.setOnClickListener(this);
        cardSettings.setOnClickListener(this);

        sharedPref = this.getSharedPreferences("viruscontrol", Context.MODE_PRIVATE);
        String dtoLoginJson = sharedPref.getString("dtoLogin", "");
        if (dtoLoginJson.isEmpty()) {
            Log.d(TAG, "onCreate: Must Login ");
            goLoginActivity();
        } else {
            try {
                DTOLogin dtoLogin = objectMapper.readValue(dtoLoginJson, DTOLogin.class);
                Session.getInstace().setLogin(dtoLogin);
                String dtoFrontProfileJson = sharedPref.getString("dtoProfile", "");
                DTOFrontProfile dtoFrontProfile = objectMapper.readValue(dtoFrontProfileJson, DTOFrontProfile.class);
                Session.getInstace().setProfile(dtoFrontProfile);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            userController.getProfile().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<DTOFrontProfile>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onSuccess(DTOFrontProfile dtoFrontProfile) {
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.i("VIRUSCONTROL", "Error: " + e.getMessage());
                            if (e instanceof VirusControlException) {
                                VirusControlException vExc = (VirusControlException) e;
                                if (vExc.getCode() == 401) {
                                    goLoginActivity();
                                }
                            }
                        }
                    });

        }
        subscribeToFirebaseTopic();
    }

    private void countNotifications() {
        unreadNotifications = 0;
        String externalId = Session.getInstace().getProfile().getExternalId();
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Task<QuerySnapshot> examTask = db.collection("examNotifications")
                .whereEqualTo("externalId", externalId)
                .whereEqualTo("read", false)
                .get();

        Task<QuerySnapshot> contagionTask = db.collection("contagionNotifications")
                .whereEqualTo("externalId", externalId)
                .whereEqualTo("read", false)
                .get();

        Task<QuerySnapshot> appointmentTask = db.collection("appointmentNotifications")
                .whereEqualTo("externalId", externalId)
                .whereEqualTo("read", false)
                .get();

        Task<QuerySnapshot> resourceTask = db.collection("resourceNotifications")
                .whereEqualTo("externalId", externalId)
                .whereEqualTo("read", false)
                .get();

        Tasks.whenAllSuccess(appointmentTask, contagionTask, examTask, resourceTask).addOnSuccessListener(list -> {
            if (list != null) {
                for (Object o : list) {
                    QuerySnapshot query = (QuerySnapshot) o;
                    unreadNotifications += query.getDocuments().size();
                }
                mNotificationNum.setText(String.valueOf(unreadNotifications));
            }
        });

    }

    private void subscribeToFirebaseTopic() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(HomeActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                Log.d("FCM Token", token);
            }
        });
        FirebaseMessaging.getInstance().subscribeToTopic(Session.getInstace().getProfile().getExternalId()).addOnCompleteListener(task -> {
            Log.d(TAG, "Subscripto al topico");
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        String dtoLoginJson = sharedPref.getString("dtoLogin", "");
        if (dtoLoginJson.isEmpty()) {
            Log.d(TAG, "onResume: Must Login ");
            goLoginActivity();
        } else {
            countNotifications();
        }
        LocationTrack.initialize(this);
        locationTrack = LocationTrack.getInstance();
        boolean isOn = sharedPref.getBoolean("isOn", true);
        boolean isManual = sharedPref.getBoolean("isManual", true);
        if(isOn && !isManual){
            handleLocation(false);
        }
        if(sharedPref.getBoolean("isSpanishSelected", true)){
            LanguageChange.setLocale(this, "es");
            cardMedic.setBackground(getResources().getDrawable(R.drawable.btn_home_medic_states));
            cardNotification.setBackground(getResources().getDrawable(R.drawable.btn_home_notification_states));
            cardLocation.setBackground(getResources().getDrawable(R.drawable.btn_home_location_states));
            cardSettings.setBackground(getResources().getDrawable(R.drawable.btn_home_settings_states));
        } else {
            LanguageChange.setLocale(this,"en");
            cardMedic.setBackground(getResources().getDrawable(R.drawable.btn_home_medic_states_english));
            cardNotification.setBackground(getResources().getDrawable(R.drawable.btn_home_notification_states_english));
            cardLocation.setBackground(getResources().getDrawable(R.drawable.btn_home_location_states_english));
            cardSettings.setBackground(getResources().getDrawable(R.drawable.btn_home_settings_states_english));
        }
    }

    private void goLoginActivity() {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }

    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()) {
            case R.id.card_medic:
                i = new Intent(this, MedicActivity.class);
                startActivity(i);
                break;
            case R.id.card_location:
                i = new Intent(this, LocationReportActivity.class);
                startActivity(i);
                break;
            case R.id.card_notification:
                i = new Intent(this, NotificationActivity.class);
                startActivity(i);
                break;
            case R.id.card_settings:
                i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                break;
            default:
                break;
        }
    }

    private void handleLocation(Boolean isManual) {
        try {
            locationTrack.setManualReport(isManual);
            locationTrack.reportLocation();
        } catch (VirusControlException e) {
            if (e.getCode() == -1) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "get permissions! ");
                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    }, REQUEST_LOCATION);
                }
            } else {
                askGPSAccess();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            Log.d(TAG, "onRequestPermissionsResult: " + Arrays.toString(grantResults));

            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("Location permissions granted, starting location");
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Por favor, habilite los permisos de ubicacion", Toast.LENGTH_SHORT);
                    toast.setMargin(50, 50);
                    toast.show();
                }
            }
        }
    }

    private void askGPSAccess() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle(getResources().getString(R.string.dialog_location_service_required));
        alertDialog.setMessage(getResources().getString(R.string.dialog_location_service_ask));

        alertDialog.setPositiveButton(getResources().getString(R.string.confirmation), (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            this.startActivity(intent);
        });

        alertDialog.setNegativeButton(getResources().getString(R.string.negation), (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }
}