package com.tsefing.viruscontrol.ui;

import androidx.appcompat.app.AppCompatActivity;
import com.tsefing.viruscontrol.R;
import com.tsefing.viruscontrol.controller.NotificationController;
import com.tsefing.viruscontrol.controller.UserController;
import com.tsefing.viruscontrol.utils.LanguageChange;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import uy.viruscontrol.common.dto.DTOFrontProfile;
import uy.viruscontrol.common.dto.DTONotifyStyle;

public class SettingsActivity extends AppCompatActivity {
    private CheckBox mCheckSpanish, mCheckEnglish, mCheckPushNotifications, mCheckMailNotifications;
    private TextView mTxtMailNotifications, mTxtPushNotifications;
    private View mCircleSpanishSelected, mCircleEnglishSelected;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private UserController userController = new UserController();
    private NotificationController notifyController = new NotificationController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        sharedPref = this.getSharedPreferences("viruscontrol", Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        mCircleEnglishSelected = findViewById(R.id.circle_english_selected);
        mCircleSpanishSelected = findViewById(R.id.circle_spanish_selected);
        mCheckSpanish = findViewById(R.id.check_spanish);
        mCheckEnglish = findViewById(R.id.check_english);
        mCheckMailNotifications = findViewById(R.id.checkBox_mail_notifications);
        mCheckPushNotifications = findViewById(R.id.checkBox_push_notifications);
        mTxtMailNotifications = findViewById(R.id.txt_mail_notifications);
        mTxtPushNotifications = findViewById(R.id.txt_push_notifications);

        userController.getProfile().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DTOFrontProfile>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(DTOFrontProfile dtoFrontProfile) {
                        mCheckMailNotifications.setChecked(dtoFrontProfile.isMailNotify());
                        mCheckPushNotifications.setChecked(dtoFrontProfile.isPushNotify());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("VIRUSCONTROL", "Error: " + e.getMessage());
                    }
                });


        mCheckSpanish.setChecked(sharedPref.getBoolean("isSpanishSelected", true));
        mCheckEnglish.setChecked(sharedPref.getBoolean("isEnglishSelected", false));

        if (mCheckSpanish.isChecked()) {
            mCheckSpanish.setScaleX(1.1f);
            mCheckSpanish.setScaleY(1.1f);
            mCheckEnglish.setScaleX(1.0f);
            mCheckEnglish.setScaleY(1.0f);
            mCircleSpanishSelected.setVisibility(View.VISIBLE);
            mCircleEnglishSelected.setVisibility(View.GONE);
            LanguageChange.setLocale(this, "es");
        } else {
            mCheckEnglish.setScaleX(1.1f);
            mCheckEnglish.setScaleY(1.1f);
            mCheckSpanish.setScaleX(1.0f);
            mCheckSpanish.setScaleY(1.0f);
            mCircleSpanishSelected.setVisibility(View.GONE);
            mCircleEnglishSelected.setVisibility(View.VISIBLE);
            LanguageChange.setLocale(this,"en");
        }

        checkLanguage();
        checkMailNotifications();
        checkPushNotifications();
    }

    private void checkLanguage(){
        mCheckSpanish.setOnClickListener(v -> {
            mCheckSpanish.setScaleX(1.1f);
            mCheckSpanish.setScaleY(1.1f);
            mCheckEnglish.setChecked(false);
            mCheckEnglish.setScaleX(1.0f);
            mCheckEnglish.setScaleY(1.0f);
            editor.putBoolean("isSpanishSelected", true);
            editor.putBoolean("isEnglishSelected", false);
            mCircleSpanishSelected.setVisibility(View.VISIBLE);
            mCircleEnglishSelected.setVisibility(View.GONE);
            LanguageChange.setLocale(this, "es");
            editor.commit();
            restartSettingsActivity();
        });

        mCheckEnglish.setOnClickListener(v -> {
            mCheckEnglish.setScaleX(1.1f);
            mCheckEnglish.setScaleY(1.1f);
            mCheckSpanish.setChecked(false);
            mCheckSpanish.setScaleX(1.0f);
            mCheckSpanish.setScaleY(1.0f);
            editor.putBoolean("isSpanishSelected", false);
            editor.putBoolean("isEnglishSelected", true);
            mCircleSpanishSelected.setVisibility(View.GONE);
            mCircleEnglishSelected.setVisibility(View.VISIBLE);
            LanguageChange.setLocale(this,"en");
            editor.commit();
            restartSettingsActivity();
        });
    }

    private void restartSettingsActivity(){
        Intent i = new Intent(SettingsActivity.this, SettingsActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        this.overridePendingTransition(0, 0);
        finish();
    }

    private void checkMailNotifications() {
        mCheckMailNotifications.setOnClickListener(v -> {
            changeNotifyStyle();
        });

        mTxtMailNotifications.setOnClickListener(v -> {
            mCheckMailNotifications.setChecked(!mCheckMailNotifications.isChecked());
            changeNotifyStyle();
        });
    }

    private void checkPushNotifications() {
        mCheckPushNotifications.setOnClickListener(v -> {
            changeNotifyStyle();
        });

        mTxtPushNotifications.setOnClickListener(v -> {
            mCheckPushNotifications.setChecked(!mCheckPushNotifications.isChecked());
            changeNotifyStyle();
        });
    }

    private void changeNotifyStyle(){
        DTONotifyStyle dtoNotifyStyle = new DTONotifyStyle();
        dtoNotifyStyle.setMailNotify(mCheckMailNotifications.isChecked());
        dtoNotifyStyle.setPushNotify(mCheckPushNotifications.isChecked());
        notifyController.changeNotifyStyle(dtoNotifyStyle)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe();
    }
}