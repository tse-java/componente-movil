package com.tsefing.viruscontrol.ui;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.FirebaseFirestore;
import com.tsefing.viruscontrol.R;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {
    private final String TAG = "VIRUSCONTROL";
    private List<NotificationItem> mNotifications;
    private NotificationAdapter.OnItemClickListener mListener;
    private NotificationItem mRecentlyDeletedItem;
    private int mRecentlyDeletedItemPosition;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    Context mContext;

    public interface OnItemClickListener {
        void onDeleteClick(int position);
    }

    public void setOnItemClickListener(NotificationAdapter.OnItemClickListener listener) {
        mListener = listener;
    }

    public static class NotificationViewHolder extends RecyclerView.ViewHolder {
        public ImageView mIcon;
        public TextView mTextView;

        public NotificationViewHolder(@NonNull View itemView, final NotificationAdapter.OnItemClickListener listener) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.tv_notification);
            mIcon = itemView.findViewById(R.id.icon_notification);
        }
    }

    public NotificationAdapter(Context context, List<NotificationItem> notificationList) {
        mContext = context;
        mNotifications = notificationList;
    }

    @NonNull
    @Override
    public NotificationAdapter.NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_notification_add_item, parent, false);
        return new NotificationAdapter.NotificationViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.NotificationViewHolder holder, int position) {
        NotificationItem currentNotification = mNotifications.get(position);
        holder.mTextView.setText(currentNotification.getNotification());
        holder.mIcon.setImageDrawable(mContext.getResources().getDrawable(currentNotification.getIcon()));
        ;
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    public void deleteItem(int position) {
        NotificationItem item = mNotifications.get(position);
        Map<String, Object> updates = new HashMap<>();
        updates.put("read", true);
        db.collection(item.getType()).document(item.getId())
                .update(updates)
                .addOnSuccessListener(aVoid -> {
                    mRecentlyDeletedItem = mNotifications.get(position);
                    mRecentlyDeletedItemPosition = position;
                    mNotifications.remove(position);
                    notifyItemRemoved(position);
                    showUndoSnackbar();
                    Log.d(TAG, "Eliminado");
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, "Error al eliminar la notificacion: "+e.getMessage());
                });
    }

    private void showUndoSnackbar() {
        View view = ((Activity) mContext).findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(view, "Notificacion eliminada",
                Snackbar.LENGTH_LONG);
        snackbar.setAction("DESHACER ELIMINACION", v -> undoDelete());
        snackbar.show();
    }

    private void undoDelete() {
        NotificationItem item = mRecentlyDeletedItem;
        Map<String, Object> updates = new HashMap<>();
        updates.put("read", false);
        db.collection(item.getType()).document(item.getId())
                .update(updates)
                .addOnSuccessListener(aVoid -> {
                    mNotifications.add(mRecentlyDeletedItemPosition, mRecentlyDeletedItem);
                    notifyItemInserted(mRecentlyDeletedItemPosition);
                    Log.d(TAG, "Resturado");
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, "Error al resturar la notificacion: "+e.getMessage());
                });
    }
}
