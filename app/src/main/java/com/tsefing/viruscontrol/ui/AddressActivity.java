package com.tsefing.viruscontrol.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.view.ViewCompat;
import com.tsefing.viruscontrol.R;
import com.tsefing.viruscontrol.controller.MedicalAppointmentController;
import com.tsefing.viruscontrol.utils.Session;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import uy.viruscontrol.common.dto.*;
import uy.viruscontrol.common.enumerations.UruguayanDepartment;
import java.util.ArrayList;
import java.util.List;
import static androidx.constraintlayout.widget.Constraints.TAG;

public class AddressActivity extends AppCompatActivity {
    private TextView mTV_MyAddress;
    private TextView mTV_AnotherAddress;
    private TextView mTV_UserProfileAddress;
    private CheckBox mCB_CheckMyAddress;
    private CheckBox mCB_CheckAnotherAddress;
    private EditText mET_AnotherUserAddress;
    private EditText mET_City;
    private Spinner mSP_Departments;
    private InputMethodManager imm;
    private List<String> mDepartments = new ArrayList<>();
    private int mDepartmentID;
    private TextView mTV_spinnerItem;
    private Button mBtn_SendAppointment;
    private boolean mEnableSendAppointment = false;
    private boolean mAnotherAddresEmpty = true;
    private boolean mCityEmpty = true;
    private boolean mDepartmentIsSelected = false;
    private MedicalAppointmentController medAppController = new MedicalAppointmentController();
    private List<Long> mSymptoms;
    private DTOFrontProfile mUser;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        mTV_MyAddress = findViewById(R.id.my_address);
        mTV_AnotherAddress = findViewById(R.id.another_address);
        mCB_CheckMyAddress = findViewById(R.id.check_my_address);
        mCB_CheckAnotherAddress = findViewById(R.id.check_another_address);
        mET_AnotherUserAddress = findViewById(R.id.address_input);
        mET_City = findViewById(R.id.city_input);
        mSP_Departments = findViewById(R.id.sp_departments);
        mTV_UserProfileAddress = findViewById(R.id.address_user_profile);
        mBtn_SendAppointment = findViewById(R.id.btn_send_appointment);
        mUser = Session.getInstace().getProfile();

        mTV_UserProfileAddress.setText(mUser.getAddress().getStreet());

        handleGetDepartments();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, mDepartments);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSP_Departments.setAdapter(adapter);
        mSP_Departments.setSelection(0);
        mSP_Departments.setEnabled(false);
        ViewCompat.setBackgroundTintList(mSP_Departments, ColorStateList.valueOf(getColor(R.color.colorGrey)));

        mSP_Departments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mDepartmentID = mSP_Departments.getSelectedItemPosition();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        mTV_spinnerItem = (TextView) mSP_Departments.getChildAt(0);

        checkAddress();

        mSymptoms = (ArrayList<Long>) getIntent().getSerializableExtra("mSymptomsList");

        mBtn_SendAppointment.setOnClickListener(v -> {
            prepareMedicalAppointment();
        });
    }

    private void handleGetDepartments() {
        mDepartments.add(getResources().getString(R.string.select_department));
        mDepartments.add("ARTIGAS");
        mDepartments.add("CANELONES");
        mDepartments.add("CERRO LARGO");
        mDepartments.add("COLONIA");
        mDepartments.add("DURAZNO");
        mDepartments.add("FLORES");
        mDepartments.add("FLORIDA");
        mDepartments.add("LAVALLEJA");
        mDepartments.add("MALDONADO");
        mDepartments.add("MONTEVIDEO");
        mDepartments.add("PAYSANDU");
        mDepartments.add("RIO NEGRO");
        mDepartments.add("RIVERA");
        mDepartments.add("ROCHA");
        mDepartments.add("SALTO");
        mDepartments.add("SAN JOSE");
        mDepartments.add("SORIANO");
        mDepartments.add("TACUAREMBO");
        mDepartments.add("TREINTA Y TRES");
    }

    public void checkAddress(){
        //Listeners para opcion de direccion actual u otra
        //mMyAddress por si toca el textview, mCheckMyAddress por si toca el checkbox
        checkMyAddress(mTV_MyAddress);
        checkMyAddress(mCB_CheckMyAddress);
        //idem a lo anterior pero con el caso de "otra direccion"
        checkAnotherAddress(mTV_AnotherAddress);
        checkAnotherAddress(mCB_CheckAnotherAddress);

        if(mEnableSendAppointment) {
            mBtn_SendAppointment.setEnabled(true);
        }
    }

    public void checkMyAddress(View checkMyAddress){
        checkMyAddress.setOnClickListener(v -> {
            mCB_CheckMyAddress.setChecked(true);
            mCB_CheckAnotherAddress.setChecked(false);
            mET_AnotherUserAddress.setEnabled(false);
            mET_City.setEnabled(false);
            mSP_Departments.setEnabled(false);
            mET_AnotherUserAddress.setTextColor(getColor(R.color.colorGrey));
            mET_City.setTextColor(getColor(R.color.colorGrey));
            mTV_UserProfileAddress.setTextColor(getColor(R.color.colorWhite));

            ViewCompat.setBackgroundTintList(mSP_Departments, ColorStateList.valueOf(getColor(R.color.colorGrey)));
            mTV_spinnerItem = (TextView) mSP_Departments.getChildAt(0);
            mBtn_SendAppointment.setEnabled(true);
        });
    }

    public void checkAnotherAddress(View checkAnotherAddress){
        imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        checkAnotherAddress.setOnClickListener(v -> {
            mBtn_SendAppointment.setEnabled(false);
            mCB_CheckMyAddress.setChecked(false);
            mCB_CheckAnotherAddress.setChecked(true);
            mET_AnotherUserAddress.setEnabled(true);
            mET_City.setEnabled(true);
            mSP_Departments.setEnabled(true);
            mET_AnotherUserAddress.requestFocus();
            mET_AnotherUserAddress.setTextColor(getColor(R.color.colorWhite));
            mET_City.setTextColor(getColor(R.color.colorWhite));
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            mTV_UserProfileAddress.setTextColor(getColor(R.color.colorGrey));
            ViewCompat.setBackgroundTintList(mSP_Departments, ColorStateList.valueOf(getColor(R.color.colorWhite)));

            checkInputsEmptyness();
        });
    }

    private void checkInputsEmptyness(){
        mET_AnotherUserAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().trim().length()==0){
                    mAnotherAddresEmpty = true;
                } else {
                    mAnotherAddresEmpty = false;
                }
                checkEmptyness ();
            }
        });

        mET_City.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {  }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().trim().length()==0){
                    mCityEmpty = true;
                } else {
                    mCityEmpty = false;
                }
                checkEmptyness ();
            }
        });

        mSP_Departments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    mDepartmentIsSelected = false;
                    mDepartmentID = 0;
                } else {
                    mDepartmentIsSelected = true;
                    mDepartmentID = i;
                }
                checkEmptyness();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
    }

    private void checkEmptyness (){
        if (!mCityEmpty && !mAnotherAddresEmpty && mDepartmentIsSelected) {
            mBtn_SendAppointment.setEnabled(true);
        } else {
            mBtn_SendAppointment.setEnabled(false);
        }
    }

    private void prepareMedicalAppointment() {
        DTONewMedicalAppointment dtoNewMedicalAppointment = new DTONewMedicalAppointment();
        DTOLocation dtoLocation = new DTOLocation();
        if (mCB_CheckMyAddress.isChecked()) {
            dtoLocation.setStreet(mUser.getAddress().getStreet());
            dtoLocation.setCity(mUser.getAddress().getCity());
            dtoLocation.setDepartment(mUser.getAddress().getDepartment());
        } else {
            dtoLocation.setStreet(mET_AnotherUserAddress.getText().toString());
            dtoLocation.setCity(mET_City.getText().toString());

            String department = mDepartments.get(mDepartmentID);
            department = department.replaceAll(" ", "_");
            dtoLocation.setDepartment(UruguayanDepartment.valueOf(department));
        }

        dtoNewMedicalAppointment.setLocation(dtoLocation);
        dtoNewMedicalAppointment.setSymptomIds(mSymptoms);
        sendMedicalAppointment(dtoNewMedicalAppointment);
    }

    private void sendMedicalAppointment(DTONewMedicalAppointment dtoNewMedicalAppointment) {
        medAppController.doNewMedicalAppointment(dtoNewMedicalAppointment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> Log.d(TAG, "error: "))
                .doOnSuccess(Boolean -> {
                    Context context = getApplicationContext();
                    LayoutInflater inflater = getLayoutInflater();
                    View toastRoot = inflater.inflate(R.layout.toast_customized, null);
                    Toast toast = new Toast(context);

                    toast.setView(toastRoot);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP,0, 150);
                    toast.show();

                    Intent i = new Intent(this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                })
                .subscribe();
    }
}
