package com.tsefing.viruscontrol.restservice;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import uy.viruscontrol.common.dto.DTOFrontProfile;
import uy.viruscontrol.common.dto.DTOLogin;
import uy.viruscontrol.common.dto.DTOToken;

public interface UserService {
    @GET("users/profile")
    Call<DTOFrontProfile> getProfile();

    @POST("users/login")
    Call<DTOLogin> login(@Body DTOToken token);

    @POST("users/refreshToken")
    Call<DTOLogin> refreshToken(@Body DTOToken token);
}
