package com.tsefing.viruscontrol.restservice;

import retrofit2.Call;
import retrofit2.http.GET;
import uy.viruscontrol.common.dto.DTOSymptom;
import java.util.List;

public interface SymptomService {
    @GET("symptoms")
    Call<List<DTOSymptom>> symptomsList();
}
