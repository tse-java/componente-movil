package com.tsefing.viruscontrol.restservice;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PATCH;
import uy.viruscontrol.common.dto.DTONotifyStyle;

public interface NotificationService {
    @PATCH("users/notification")
    Call<Void> changeNotifyStyle(@Body DTONotifyStyle dtoNotifyStyle);
}
