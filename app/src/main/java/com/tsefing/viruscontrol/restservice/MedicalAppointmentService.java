package com.tsefing.viruscontrol.restservice;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import uy.viruscontrol.common.dto.DTONewMedicalAppointment;

public interface MedicalAppointmentService {
    @POST("appointments")
    Call<Void> doNewMedicalAppointment(@Body DTONewMedicalAppointment dtoNewMedicalAppointment);
}
