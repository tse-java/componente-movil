package com.tsefing.viruscontrol.restservice;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import uy.viruscontrol.common.dto.DTODistance;
import uy.viruscontrol.common.dto.DTOLocationReport;

public interface LocationService {
    @POST("location")
    Call<Void> reportLocation(@Body DTOLocationReport dtoLocationReport);

    @GET("location")
    Call<DTODistance> getContagionDistance();
}
