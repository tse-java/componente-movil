package com.tsefing.viruscontrol.controller;

import android.util.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsefing.viruscontrol.restservice.UserService;
import com.tsefing.viruscontrol.utils.HttpClient;
import java.io.IOException;
import java.util.Map;
import io.reactivex.Single;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uy.viruscontrol.common.dto.DTOFrontProfile;
import uy.viruscontrol.common.dto.DTOLogin;
import uy.viruscontrol.common.dto.DTOToken;
import uy.viruscontrol.common.exceptions.VirusControlException;

public class UserController {

    private final String TAG = "VIRUSCONTROL";

    private final ObjectMapper objectMapper = new ObjectMapper();

    private UserService userService = HttpClient.retrofit.create(UserService.class);

    public UserController() { }

    public Single<String> googleLogin(String code) {
        return Single.create(emitter -> {
            FormBody.Builder builder = new FormBody.Builder();
            builder.add("grant_type", "authorization_code")
                    .add("client_id", "884442857108-q9a1vj0f8r97ku0e2bb3de8kprmb1del.apps.googleusercontent.com")
                    .add("client_secret", "cHlNc5wEoCAsVAzVnxFhjr-n")
                    .add("redirect_uri", "")
                    .add("code", code);
            RequestBody formBody = builder.build();
            Request request = new Request.Builder()
                    .url("https://oauth2.googleapis.com/token")
                    .post(formBody)
                    .build();
            try {
                Response googleResponse = new OkHttpClient().newCall(request).execute();
                String googleJson = googleResponse.body().string();
                Map mapGoogle = objectMapper.readValue(googleJson, Map.class);
                emitter.onSuccess((String) mapGoogle.get("access_token"));
            } catch (IOException e) {
                emitter.onError(new VirusControlException("Error al iniciar sesion con google.", VirusControlException.CANT_CONNECT));
            }
        });
    }

    public Single<DTOLogin> viruscontrolLogin(DTOToken token) {
        return Single.create(emitter -> {
            retrofit2.Response<DTOLogin> response = userService.login(token).execute();
            if (response.isSuccessful()) {
                if (response.body().getFinishedUserProfile()) {
                    emitter.onSuccess(response.body());
                } else {
                    emitter.onError(new VirusControlException("Usuario debe terminar de configurar su cuenta accediendo a https://viruscontrol.tech", -1));
                }
            } else {
                Log.i(TAG, response.errorBody().string());
                emitter.onError(new VirusControlException(response.errorBody().string(), response.code()));
            }
        });
    }

    public Single<DTOFrontProfile> getProfile() {
        return Single.create(emitter -> {
            retrofit2.Response<DTOFrontProfile> response = userService.getProfile().execute();
            if (response.isSuccessful()) {
                    emitter.onSuccess(response.body());
            } else {
                Log.i(TAG, response.errorBody().string());
                emitter.onError(new VirusControlException(response.errorBody().string(), response.code()));
            }
        });
    }
}