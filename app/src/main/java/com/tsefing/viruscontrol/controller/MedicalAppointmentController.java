package com.tsefing.viruscontrol.controller;

import android.util.Log;
import com.tsefing.viruscontrol.restservice.MedicalAppointmentService;
import com.tsefing.viruscontrol.utils.HttpClient;
import io.reactivex.Single;
import retrofit2.Response;
import uy.viruscontrol.common.dto.DTONewMedicalAppointment;
import uy.viruscontrol.common.exceptions.VirusControlException;

public class MedicalAppointmentController {
    private final String TAG = "VIRUSCONTROL";
    private MedicalAppointmentService service = HttpClient.retrofit.create(MedicalAppointmentService.class);

    public MedicalAppointmentController() { }

    public Single<Boolean> doNewMedicalAppointment(DTONewMedicalAppointment dtoNewMedicalAppointment) {
        return Single.create(emitter -> {
            try {
                Response<Void> response = service.doNewMedicalAppointment(dtoNewMedicalAppointment).execute();
                if (response.isSuccessful()) {
                    emitter.onSuccess(true);
                } else {
                    assert response.errorBody() != null;
                    Log.i(TAG, response.errorBody().string());
                    emitter.onError(new VirusControlException(response.errorBody().string(), response.code()));
                }
            }catch (Exception e){
                emitter.onError(new VirusControlException(e.getMessage(), -1));
            }
        });
    }
}