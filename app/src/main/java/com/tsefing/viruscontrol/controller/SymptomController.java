package com.tsefing.viruscontrol.controller;

import android.util.Log;
import com.tsefing.viruscontrol.restservice.SymptomService;
import com.tsefing.viruscontrol.utils.HttpClient;
import io.reactivex.Single;
import retrofit2.Response;
import uy.viruscontrol.common.dto.DTOSymptom;
import uy.viruscontrol.common.exceptions.VirusControlException;
import java.util.List;

public class SymptomController {
    private final String TAG = "VIRUSCONTROL";
    private SymptomService service = HttpClient.retrofit.create(SymptomService.class);

    public SymptomController() {}

    public Single<List<DTOSymptom>> getSymptoms() {
        return Single.create(emitter -> {
            Response<List<DTOSymptom>> response = service.symptomsList().execute();
            if (response.isSuccessful()){
                emitter.onSuccess(response.body());
            } else {
                Log.i(TAG, response.errorBody().string());
                emitter.onError(new VirusControlException(response.errorBody().string(), response.code()));
            }
        });
    }
}