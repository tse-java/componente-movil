package com.tsefing.viruscontrol.controller;

import android.util.Log;
import com.tsefing.viruscontrol.restservice.LocationService;
import com.tsefing.viruscontrol.utils.HttpClient;
import io.reactivex.Maybe;
import io.reactivex.Single;
import retrofit2.Response;
import uy.viruscontrol.common.dto.DTODistance;
import uy.viruscontrol.common.dto.DTOLocationReport;
import uy.viruscontrol.common.exceptions.VirusControlException;

public class LocationController {
    private final String TAG = "VIRUSCONTROL";

    private LocationService service = HttpClient.retrofit.create(LocationService.class);

    public LocationController() { }

    public Maybe<Void> reportLocation(DTOLocationReport dtoLocationReport) {
        return Maybe.create(emitter -> {
            Response<Void> response = service.reportLocation(dtoLocationReport).execute();
            if (response.isSuccessful()) {
                emitter.onComplete();
            } else {
                Log.i(TAG, response.errorBody().string());
                emitter.onError(new VirusControlException(response.errorBody().string(), response.code()));
            }
        });
    }

    public Single<DTODistance> getContagionDistance() {
        return Single.create(emitter -> {
            Response<DTODistance> response = service.getContagionDistance().execute();
            if (response.isSuccessful()){
                emitter.onSuccess(response.body());
            } else {
                Log.i(TAG, response.errorBody().string());
                emitter.onError(new VirusControlException(response.errorBody().string(), response.code()));
            }
        });
    }
}