package com.tsefing.viruscontrol.controller;

import android.util.Log;
import com.tsefing.viruscontrol.restservice.NotificationService;
import com.tsefing.viruscontrol.utils.HttpClient;
import io.reactivex.Maybe;
import retrofit2.Response;
import uy.viruscontrol.common.dto.DTONotifyStyle;
import uy.viruscontrol.common.exceptions.VirusControlException;

public class NotificationController {
    private final String TAG = "VIRUSCONTROL";
    private NotificationService service = HttpClient.retrofit.create(NotificationService.class);

    public NotificationController(){}

    public Maybe<Void> changeNotifyStyle(DTONotifyStyle dtoNotifyStyle) {
        return Maybe.create(emitter -> {
            Response<Void> response = service.changeNotifyStyle(dtoNotifyStyle).execute();
            if (response.isSuccessful()) {
                emitter.onComplete();
            } else {
                Log.i(TAG, response.errorBody().string());
                emitter.onError(new VirusControlException(response.errorBody().string(), response.code()));
            }
        });
    }
}